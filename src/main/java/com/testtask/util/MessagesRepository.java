package com.testtask.util;

import com.testtask.entity.Message;
import org.apache.log4j.Logger;

import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

public class MessagesRepository {
    private static Logger log = Logger.getLogger(MessagesRepository.class.getName());
    private LinkedList<Message> messages;
    private int maxMessagesToKeep;
    private int messageId = 0;

    public MessagesRepository() {
        messages = new LinkedList<>();
    }

    public void setMaxMessagesToKeep(int maxMessagesToKeep) {
        this.maxMessagesToKeep = maxMessagesToKeep;
    }

    public void addMessageToRepo(Message message) {
        synchronized (this) {
            message.setId(++messageId);
//            Message reEncodedMessage = new Message();
//            reEncodedMessage.setId(++messageId);
//            reEncodedMessage.setNickname(reEncode(message.getNickname()));
//            reEncodedMessage.setMessage(reEncode(message.getMessage()));
            messages.add(message);
            log.info(message);
            if (messages.size() > maxMessagesToKeep) {
                messages.removeFirst();
            }
        }
        log.debug("Added a message: " + message);
    }

    public List<Message> getNewMessages(int lastMessageId) {
        synchronized (this) {
            if (lastMessageId >= messageId) {
                log.warn("lastMessageId >= messageId");
                return new LinkedList<Message>();
            }
            else if (lastMessageId < messages.getFirst().getId()) {
                return messages;
            }
            else {
                return messages.subList(lastMessageId - messages.getFirst().getId() + 1, messages.size());
            }
        }
    }

    //Spring MVC uses ISO-8859-1 as charset for requests, so we reencode message to correct charset
    private String reEncode(String input) {
        Charset w1252 = Charset.forName("ISO-8859-1");
        Charset utf8 = Charset.forName("UTF-8");
        return new String(input.getBytes(w1252), utf8 );
    }
}
