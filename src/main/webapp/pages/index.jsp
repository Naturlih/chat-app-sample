<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/ChatApp/css/css.css">
    <title>Чат</title>
    <script src="/ChatApp/js/jquery-2.1.1.min.js" type="text/javascript" ></script>
</head>
<body>
    <table>
        <tr>
            <td>
                <label for="messageInput">Сообщение</label>
            </td>
            <td>
                <input id="messageInput" name="message">
            </td>
        </tr>
        <tr>
            <td>
                <label for="nicknameInput">Никнейм</label>
            </td>
            <td>
                <input id="nicknameInput" name="nickname" value="Гость">
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button onclick="sendMessage()">Послать сообщение</button>
            </td>
        </tr>
    </table>
    <table>
            <tr>
                <td>
                    <button onclick="getNewMessages();">Обновить чат</button>
                </td>
            </tr>
        <tr>
            <td>
                <textarea id="chatArea" class="chat_area"></textarea>
            </td>
        </tr>
    </table>
</body>
<script>
    var lastMessageId = 0;
    var chatArea = document.getElementById("chatArea");
    window.onbeforeunload = function() {
        localStorage.setItem("nickname", document.getElementById("nicknameInput").value);
    };
    loadNickname();
    getNewMessages();

    function sendMessage() {
        $.ajax({
            url: '/ChatApp/sendMessage',
            type:'POST',
            data:
            {
                nickname: document.getElementById("nicknameInput").value,
                message: document.getElementById("messageInput").value,
                id: 0
            },
            complete: function(msg)
            {
                getNewMessages();
            }
        });
    }

    function getNewMessages() {
        $.get('/ChatApp/newMessages?lastMessageId=' + lastMessageId ,function(data, status){
            var chat = '';
            for (var i = 0; i < data.length; i++) {
                chat += '[' + data[i].nickname + ']: ' + data[i].message + '\n';
            }
            if (data.length > 0 && (lastMessageId + 1) < data[0].id) {
                chatArea.value += '...\n';
            }
            chatArea.value += chat;
            if (data.length > 0) {
                lastMessageId = data[data.length - 1].id;
            }
        });
    }

    function loadNickname() {
        var nickname = localStorage.getItem("nickname");
        if (nickname !== null) document.getElementById("nicknameInput").value = nickname;
    }
</script>
</html>