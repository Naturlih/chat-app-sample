package com.testtask.controller;

import com.testtask.entity.Message;
import com.testtask.util.MessagesRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Controller
public class MainController{
    static Logger log = Logger.getLogger(MainController.class.getName());

    @Autowired
    MessagesRepository messagesRepository;

    @RequestMapping(value = "/chat", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
    public String sendIndexPage() {
        log.debug("GET request");
        return "index";
    }

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
    public void getMessage(Message message) {
        log.info("POST request " + message);
        messagesRepository.addMessageToRepo(message);
    }

    @RequestMapping(value = "/newMessages", method = RequestMethod.GET)
    @ResponseBody
    public List<Message> sendMessages(@RequestParam(value="lastMessageId", required=true) int lastMessageId) {
        log.info("sendMessages request");
        return messagesRepository.getNewMessages(lastMessageId);
    }
}