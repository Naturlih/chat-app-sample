package com.testtask.entity;

public class Message {
    private String nickname;
    private String message;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Message() {
        nickname = "";
        message = "";
    }

    public Message(String nickname, String message, int id) {
        this.nickname = nickname;
        this.message = message;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Message{" +
                "nickname='" + nickname + '\'' +
                ", message='" + message + '\'' +
                ", id=" + id +
                '}';
    }
}
